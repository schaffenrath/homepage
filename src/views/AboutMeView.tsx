import styled from "styled-components";
import RenderInstance from "../renderer/Renderer";
import StarBeamRenderInstance from "../renderer/StarBeamRenderer";

const Background = styled.div`
  width: 100%;
  height: 100%;
  background: #414141;
  background: -moz-linear-gradient(
    top,
    #867139 20%,
    #414141 100%
  ); /* FF3.6-15 */
  background: -webkit-linear-gradient(
    top,
    #867139 20%,
    #414141 100%
  ); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(
    to bottom,
    #867139 20%,
    #414141 100%
  ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
`;
interface Props {
  navigationBar: JSX.Element;
  renderer: RenderInstance;
}
export default function AboutMeView(props: Props) {
  return (
    <Background>
      <div style={{ position: "relative" }}>
        <StarBeamRenderInstance renderer={props.renderer.getRenderInstance()} />
        {props.navigationBar}
      </div>
    </Background>
  );
}
