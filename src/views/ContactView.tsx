import styled from "styled-components";
import PlanetRenderInstance from "../renderer/PlanetRenderer";
import RenderInstance from "../renderer/Renderer";

const Background = styled.div`
  width: ${window.innerWidth}px;
  height: ${window.innerHeight}px;
  background: #414141; /* Old browsers */
  background: -moz-linear-gradient(
    top,
    #2d0d15 0%,
    #663b3b 100%
  ); /* FF3.6-15 */
  background: -webkit-linear-gradient(
    top,
    #2d0d15 0%,
    #663b3b 100%
  ); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(
    to right,
    #2d0d15 0%,
    #663b3b 100%
  ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
`;

interface Props {
  navigationBar: JSX.Element;
  renderer: RenderInstance;
}

export default function ContactView(props: Props) {
  return (
    <Background>
      <div style={{ position: "relative" }}>
        <PlanetRenderInstance renderer={props.renderer.getRenderInstance()} />
        {props.navigationBar}
      </div>
    </Background>
  );
}
