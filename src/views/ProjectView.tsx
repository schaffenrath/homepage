import styled from "styled-components";
import FlowRenderInstance from "../renderer/FlowRenderer";
import RenderInstance from "../renderer/Renderer";

const Background = styled.div`
  width: ${window.innerWidth}px;
  height: ${window.innerHeight}px;
  background: #414141;
  background: -moz-linear-gradient(
    top,
    #414141 0%,
    #142850 100%
  ); /* FF3.6-15 */
  background: -webkit-linear-gradient(
    top,
    #414141 0%,
    #142850 100%
  ); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(
    to bottom,
    #414141 0%,
    #142850 100%
  ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
`;

interface Props {
  navigationBar: JSX.Element;
  renderer: RenderInstance;
}
export default function ProjectView(props: Props) {
  return (
    <Background>
      <div style={{ position: "relative" }}>
        <FlowRenderInstance renderer={props.renderer.getRenderInstance()} />
        {props.navigationBar}
      </div>
    </Background>
  );
}
