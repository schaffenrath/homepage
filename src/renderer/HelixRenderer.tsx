import { useEffect, useRef } from "react";
import * as THREE from "three";
import { GeneralRender } from "./Renderer";

/**
 * This is an implementation of a Helix animation.
 * However, it is currently not in use.
 */

class HelixRender {
  readonly particleCount = 30000;
  readonly radius = 10;
  private plane: THREE.InstancedMesh | undefined = undefined;
  private time = 0.0;
  private offset: Array<number> = [];
  private displacement: Array<THREE.Vector3> = [];

  private rendererInstance: GeneralRender;

  constructor(renderInstance: GeneralRender) {
    this.rendererInstance = renderInstance;
    this.initHelix();
  }

  private initHelix = () => {
    this.rendererInstance.resetScene();
    this.rendererInstance.getCamera().fov = 75;
    this.rendererInstance.getCamera().position.z = 90;
    this.rendererInstance.getCamera().updateProjectionMatrix();

    const particleGeometry = new THREE.OctahedronBufferGeometry(1, 0);
    const particleMaterial = new THREE.MeshPhongMaterial({
      color: 0xffffff,
      shininess: 20,
      flatShading: true,
    });

    this.plane = new THREE.InstancedMesh(
      particleGeometry,
      particleMaterial,
      this.particleCount
    );
    this.rendererInstance.getScene().add(this.plane);

    const drawObj = new THREE.Object3D();
    drawObj.scale.set(0.1, 0.1, 0.1);
    for (let i = 0; i < this.particleCount; i++) {
      drawObj.position.set(0.0, -70.0, 0.0);
      drawObj.updateMatrix();
      this.plane.setMatrixAt(i, drawObj.matrix);
      this.offset.push(Math.random() * 31);
      this.displacement.push(
        new THREE.Vector3(
          Math.random() * 8,
          Math.random() * 8,
          Math.random() * 8
        )
      );
    }
    this.plane.instanceMatrix.needsUpdate = true;

    const ambientLight = new THREE.AmbientLight(0x6cdd2a);
    // this.rendererInstance.getScene().add(ambientLight);

    const lights = [];
    lights[0] = new THREE.DirectionalLight(0xfce731, 1);
    lights[0].position.set(0, 1, 0.5);
    lights[1] = new THREE.DirectionalLight(0x33fa4d, 1);
    lights[1].position.set(0, -1, 0.5);
    lights[2] = new THREE.DirectionalLight(0x2e70fd, 1);
    lights[2].position.set(0, 0, 1);
    this.rendererInstance.getScene().add(lights[0]);
    this.rendererInstance.getScene().add(lights[1]);
    this.rendererInstance.getScene().add(lights[2]);

    this.rendererInstance.renderScene();
    window.addEventListener(
      "resize",
      this.rendererInstance.updateCameraOnResize
    );
  };

  private animate = () => {
    this.rendererInstance.updateAnimationFrameId(
      requestAnimationFrame(this.animate)
    );
    this.time += 0.001;

    const matrix = new THREE.Matrix4();
    for (let i = 0; i < this.particleCount; i++) {
      this.plane!.getMatrixAt(i, matrix);
      const pos = new THREE.Vector3();
      pos.setFromMatrixPosition(matrix);
      if (pos.y > 30) {
        this.offset[i] = -this.time;
      }
      pos.set(
        (this.radius + this.offset[i] * 0.1) *
          Math.cos(this.time + this.offset[i]) +
          this.displacement[i].x,
        this.time - 70 + this.offset[i] * 3 + this.displacement[i].y,
        (this.radius + this.offset[i] * 0.1) *
          Math.sin(this.time + this.offset[i]) +
          this.displacement[i].z
      );
      matrix.setPosition(pos);
      this.plane!.setMatrixAt(i, matrix);
    }
    this.plane!.instanceMatrix.needsUpdate = true;
    this.rendererInstance.renderScene();
  };

  startRendering = (container: HTMLDivElement) => {
    container.appendChild(this.rendererInstance.getRenderer().domElement);
    this.animate();
  };
}

interface Props {
  renderer: GeneralRender;
}

export default function HelixRenderInstance(props: Props) {
  const containerRef = useRef<HTMLDivElement>(null);
  const waveRendererRef = useRef<HelixRender | undefined>(undefined);

  useEffect(() => {
    if (waveRendererRef.current === undefined) {
      waveRendererRef.current = new HelixRender(props.renderer);
      waveRendererRef.current.startRendering(containerRef.current!);
    }
  }, [props.renderer]);

  return <div ref={containerRef} style={{ zIndex: 0, position: "absolute" }} />;
}
