import { useCallback } from "react";
import styled from "styled-components";

import MenuButton from "../components/MenuButton";
import { ViewType } from "../views/ViewHandler";

const NameHeading = styled.h1`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  font-family: Montserrat;
  font-size: 3em;
  color: #c9c9c9;
  font-weight: 100;
  margin-top: 5%;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: 50px;
  flex-wrap: wrap;
  justify-content: center;
  text-align: center;
  margin-top: 5em;
  width: 100%;

  @media only screen and (max-width: 768px) {
    flex-direction: column;
    align-items: center;
    /* justify-content: space-space-evenly; */
  }
`;

interface Props {
  changeView: (newView: ViewType) => void;
}

export default function NavigationBar(props: Props) {
  const selectNewView = useCallback(
    (newView: ViewType) => {
      props.changeView(newView);
    },
    [props]
  );

  return (
    <div
      style={{
        position: "absolute",
        width: "100%",
      }}
    >
      <NameHeading
        onClick={() => selectNewView("HOME")}
        style={{
          cursor: "pointer",
        }}
      >
        ROBERT SCHAFFENRATH
      </NameHeading>
      <ButtonContainer>
        <MenuButton
          text="ABOUT ME"
          color="#2b8558"
          targetLocation={() => selectNewView("ABOUT_ME")}
        />
        <MenuButton
          text="PROJECTS"
          color="#501870"
          targetLocation={() => selectNewView("PROJECTS")}
        />
        <MenuButton
          text="CONTACT"
          color="#792a2a"
          targetLocation={() => selectNewView("CONTACT")}
        />
      </ButtonContainer>
    </div>
  );
}
