import styled from "styled-components";

const CustomButton = styled.div`
  display: inline-block;
  /* list-style: none; */
  width: 180px;
  height: 50px;
  line-height: 50px;
  /* vertical-align: middle; */
  font-family: Helvetica;
  letter-spacing: 0.1em;
  font-weight: 200;
  font-size: 1.8em;
  text-align: center;
  color: #b9b9b9;
  cursor: pointer;

  border-top: 1px solid;
  border-bottom: 1px solid;

  /* outline: 2px solid;
  outline-color: ${(props) => props.color};
  outline-offset: 10px;
  transition: all 600ms cubic-bezier(0.2, 0, 0, 0.8);
  */
  &:hover {
    outline: 1px solid;
    color: ${(props) => props.color};
    /* color: ${(props) => props.color};
    outline-color: rgba(71, 126, 232, 0);
    outline-offset: 300px; */
  }
`;

interface Props {
  text: string;
  color: string;
  targetLocation: () => void;
}

export default function MenuButton(props: Props) {
  return (
    <div style={{ width: "18%", height: 10 }} onMouseUp={props.targetLocation}>
      <CustomButton color={props.color}>{props.text}</CustomButton>
    </div>
  );
}
