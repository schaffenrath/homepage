import "./App.css";
import ViewHandler from "./views/ViewHandler";

function App() {
  return <ViewHandler />;
}

export default App;
